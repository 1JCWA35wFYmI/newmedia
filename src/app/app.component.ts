import { Component } from '@angular/core';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { faShoppingBag } from '@fortawesome/free-solid-svg-icons';
import { faBraille } from '@fortawesome/free-solid-svg-icons';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'NewMedia';
  faUserAlt = faUserAlt;
  faShoppingBag = faShoppingBag;
  faSearch = faSearch;
  faBraille = faBraille ;

  current_active_tab_menu = "";
}
